package com.exactpro.bootcamp.flows

import co.paralleluniverse.fibers.Suspendable
import com.exactpro.bootcamp.contracts.DollarContract
import com.exactpro.bootcamp.contracts.TokenContract
import com.exactpro.bootcamp.states.DollarState
import com.exactpro.bootcamp.states.TokenState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateRef
import net.corda.core.contracts.requireThat
import net.corda.core.crypto.SecureHash
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker

// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
class TokenSellFlowInitiator(
    private val transactionId: SecureHash,
    private val outputIndex: Int,
    private val transactionDollarId: SecureHash,
    private val outputDollarIndex: Int,
    private val transactionBuyerDollarId: SecureHash,
    private val outputBuyerDollarIndex: Int,
    private val amount: Int,
    private val cost: Int,
    private val newOwner: Party
) : FlowLogic<SignedTransaction>() {
    override val progressTracker = ProgressTracker()

    @Suspendable
    override fun call(): SignedTransaction {
        // Initiator flow logic goes here.

        // We choose our transaction's notary (the notary prevents double-spends).
        val notary = serviceHub.networkMapCache.notaryIdentities.first()

        // We get original state and reference
        val inputState = serviceHub.vaultService.queryBy<TokenState>(
            QueryCriteria.VaultQueryCriteria(stateRefs = listOf(StateRef(transactionId, outputIndex)))
        ).states.single()


        val inputDollarStateSeller = serviceHub.vaultService.queryBy<DollarState>(
            QueryCriteria.VaultQueryCriteria(stateRefs = listOf(StateRef(transactionDollarId, outputDollarIndex)))
        ).states.single()

        val inputDollarStateBuyer = serviceHub.vaultService.queryBy<DollarState>(
            QueryCriteria.VaultQueryCriteria(stateRefs = listOf(StateRef(transactionBuyerDollarId, outputBuyerDollarIndex)))
        ).states.single()

        // We get the required signers.
        val signers = inputDollarStateBuyer.state.data.participants.union(inputDollarStateSeller.state.data.participants)

        val outputOldOwnerState = inputState.state.data.copy(amount = inputState.state.data.amount - amount)

        val outputState = TokenState(
            inputState.state.data.issuer,
            newOwner,
            amount
        )

        val outputDollarState = inputDollarStateSeller.state.data.copy(amount = inputDollarStateSeller.state.data.amount + cost * amount)
        val outputDollarStateBuyer = inputDollarStateBuyer.state.data.copy(amount = inputDollarStateBuyer.state.data.amount - cost * amount)

        // We create Sell commands.
        val sellCommand = Command(TokenContract.Commands.Sell(), signers.map { it.owningKey })
        val sellCommandsDollar = Command(DollarContract.Commands.Sell(), signers.map { it.owningKey })

        // We build our transaction.
        val transactionBuilder = TransactionBuilder(notary)
            .addInputState(inputState)
            .addInputState(inputDollarStateBuyer)
            .addInputState(inputDollarStateSeller)
            .addOutputState(outputOldOwnerState, TokenContract.ID)
            .addOutputState(outputState, TokenContract.ID)
            .addOutputState(outputDollarState, DollarContract.ID)
            .addOutputState(outputDollarStateBuyer, DollarContract.ID)
            .addCommand(sellCommand)
            .addCommand(sellCommandsDollar)

        // We check our transaction is valid based on its contracts.
        transactionBuilder.verify(serviceHub)

        // We sign the transaction with our private key, making it immutable.
        val signedTransaction = serviceHub.signInitialTransaction(transactionBuilder)

        // Create sessions with the other parties.
        val sessions = (signers - ourIdentity).map { initiateFlow(it) }

        // The counterparties sign the transaction
        val fullySignedTransaction = subFlow(CollectSignaturesFlow(signedTransaction, sessions))

        // We get the transaction notarised and recorded automatically by the platform.
        return subFlow(FinalityFlow(fullySignedTransaction, sessions))
    }
}

@InitiatedBy(TokenSellFlowInitiator::class)
class TokenSellFlowResponder(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        // Responder flow logic goes here.

        val signedTransactionFlow = object: SignTransactionFlow(counterpartySession) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        }

        // The counterparty signs the transaction
        val expectedTransactionId = subFlow(signedTransactionFlow).id

        // The counterparty receives the transaction and saves the state
        subFlow(ReceiveFinalityFlow(counterpartySession, expectedTransactionId))
    }
}
