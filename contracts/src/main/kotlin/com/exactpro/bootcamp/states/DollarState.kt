package com.exactpro.bootcamp.states

import com.exactpro.bootcamp.contracts.DollarContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party

// *********
// * State *
// *********

// Our state, defining a shared fact on the ledger.
@BelongsToContract(DollarContract::class)
data class DollarState(
    val issuer: Party,
    val owner: Party,
    val amount: Int
) : ContractState {
    override val participants: List<AbstractParty>
        get() = listOf(issuer, owner)
}
